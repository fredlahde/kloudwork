package io.kloudwork.config;

public class Constants {

    /**
     * The name of the io.kloudwork.app.
     */
    public static final String APP_NAME = "Spark-Template";

    /**
     * The io.kloudwork.config file.
     *
     * Path is relative from the home directory.
     */
    public static final String CONFIG_FILE = ".spark-templates.properties";
}
